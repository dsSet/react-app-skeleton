const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const appConfig = require('../config');

const devConfig = require('./dev.config');
const prodConfig = require('./prod.config');

const ENV_PROD = 'PROD';
const ENV_DEV = 'DEV';

const rootPath = path.join(__dirname, '../');
const sourcePath = path.join(rootPath, './src');
const distPath = path.join(rootPath, './dist');
const templatePath = path.join(rootPath, './templates');

const VENDOR_FILE_NAME = 'vendor';

const vendors = ['lodash'];

const plugins = [
  new CleanWebpackPlugin(distPath, { root: rootPath }),
  new webpack.optimize.CommonsChunkPlugin({
    name: VENDOR_FILE_NAME,
    minChunks: Infinity,
    filename: `${VENDOR_FILE_NAME}.bundle.js`
  }),
  new webpack.optimize.CommonsChunkPlugin({ name: 'manifest', minChunks: Infinity }),
  new webpack.NamedModulesPlugin(),
  new HtmlWebpackPlugin({
    APP_ROOT_ID: appConfig.APP_ROOT_ID,
    template: path.resolve(templatePath, 'index.html'),
  }),
  new webpack.DefinePlugin({
    APP_ROOT_ID: appConfig.APP_ROOT_ID
  })
];

module.exports = (env) => {
  const nodeEnv = env && env.prod ? ENV_PROD : ENV_DEV;
  const config = nodeEnv === ENV_PROD ? prodConfig : devConfig;

	// eslint-disable-next-line no-console
  console.log(config.devtool);

  return {
    context: rootPath,
    devtool: config.devtool,
    entry: {
      index: path.resolve(sourcePath, 'index.js'),
      [VENDOR_FILE_NAME]: vendors,
    },
    output: {
      path: distPath,
      filename: '[name].[hash].js',
      publicPath: '/'
    },
    module: {
      rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015', 'es2016', 'flow']
        }
      }],
    },
    resolve: {
      modules: [
        path.resolve(rootPath, 'node_modules')
      ]
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env': { NODE_ENV: JSON.stringify(nodeEnv) }
      })
    ].concat(plugins, config.plugins),
    performance: config.performance,
    devServer: config.devServer,
  };
};
