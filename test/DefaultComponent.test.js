import React from 'react';
import { shallow } from 'enzyme';
import DefaultComponent from '../src/DefaultComponent';

describe('Component test sample', () => {
  it('Snapshot', () => {
    const wrapper = shallow(<DefaultComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
