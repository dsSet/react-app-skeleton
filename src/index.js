// @flow
import React from 'react';
import { render } from 'react-dom';
import DefaultComponent from './DefaultComponent';

render(<DefaultComponent />, APP_ROOT_ID);
