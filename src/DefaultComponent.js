import React from 'react';

const DefaultComponent = () => (
  <div>{'Hello world'}</div>
);

export default DefaultComponent;
